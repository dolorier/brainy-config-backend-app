package com.avantica.brainy.dao.impl;

import com.avantica.brainy.beans.Configuration;
import com.avantica.brainy.controller.Application;
import com.avantica.brainy.dao.ConfigurationDAO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ConfigurationDAOImplTest {

    @Autowired
    @Qualifier("persistenceConfigurationDAO")
    private ConfigurationDAO configurationDAO;

    @Test
    public void getAllConfigurations() throws Exception {
        configurationDAO.getAllConfigurations();
    }

    @Test
    public void updateAllConfiguration() throws Exception {
        List<Configuration> configurationList = new ArrayList<>();
        Configuration configuration = new Configuration();
        configuration.setKey("key1");
        configuration.setValue("abregu");
        configurationList.add(configuration);
        configurationDAO.updateAllConfiguration(configurationList);
    }
}
