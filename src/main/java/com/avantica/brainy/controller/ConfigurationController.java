package com.avantica.brainy.controller;


import com.avantica.brainy.beans.Configuration;
import com.avantica.brainy.beans.Message;
import com.avantica.brainy.exception.BrainyException;
import com.avantica.brainy.service.ConfigurationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@CrossOrigin
@RestController
public class ConfigurationController {

    public static final String CONFIGURATION_PATH = "/configuration";

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationController.class);

    private ConfigurationService configurationService;

    @Autowired
    public ConfigurationController(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @RequestMapping("/")
    public String index() {
        LOGGER.info("Greetings from Spring Boot!");
        return "Greetings from Spring Boot!";
    }

    @RequestMapping(value = CONFIGURATION_PATH, method = PUT)
    public ResponseEntity<Message> updateAllConfiguration(@RequestBody List<Configuration> configurationList) {
        LOGGER.info("update configuration");
        configurationService.updateAllConfiguration(configurationList);
        return new ResponseEntity<>(new Message(OK)
                ,OK);
    }

    @RequestMapping(value = CONFIGURATION_PATH, method = GET)
    public ResponseEntity<List<Configuration>> getAllConfigurations() {
        LOGGER.info("get all configurations");
        List<Configuration> listResult = configurationService.getAllConfigurations();
        return new ResponseEntity<>(listResult, OK);
    }

    @ExceptionHandler({BrainyException.class, Exception.class})
    public ResponseEntity<Message> brainyError(HttpServletRequest req, Exception ex) {
        LOGGER.error("BRAINY_EX: page:" + req.getRequestURL() + ", ex:" + ex);
        return new ResponseEntity<>(new Message(INTERNAL_SERVER_ERROR)
                ,INTERNAL_SERVER_ERROR);
    }

}
