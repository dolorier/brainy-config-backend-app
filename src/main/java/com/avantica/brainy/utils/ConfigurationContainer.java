package com.avantica.brainy.utils;

import com.avantica.brainy.beans.Configuration;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.unmodifiableList;

@Component
public class ConfigurationContainer {

    private List<Configuration> configurationList;

    public ConfigurationContainer() {
        this.configurationList = new ArrayList<>();
    }

    public boolean updateAll(List<Configuration> configurations) {
        this.configurationList = configurations;
        return true;
    }

    public List<Configuration> getAll() {
        return unmodifiableList(configurationList);
    }

    public void removeAll() {
        configurationList = new ArrayList<>();
    }

}
