package com.avantica.brainy.dao.impl;

import com.avantica.brainy.beans.Configuration;
import com.avantica.brainy.dao.ConfigurationDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository("persistenceConfigurationDAO")
public class PersistenceConfigurationDAO implements ConfigurationDAO{

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public boolean updateAllConfiguration(List<Configuration> configurationList) {
        Map<Object, Object> configurations = new HashMap<>();
        for(Configuration configuration : configurationList) {
            configurations.put(configuration.getKey(), configuration.getValue());
        }
        redisTemplate.boundHashOps("configuration").putAll(configurations);
        return true;
    }

    @Override
    public List<Configuration> getAllConfigurations() {
        Map<Object, Object> configurations =
        redisTemplate.boundHashOps("configuration").entries();
        Iterator iterator = configurations.entrySet().iterator();
        List<Configuration> configurationList = new ArrayList<>();
        while(iterator.hasNext()){
            Map.Entry pair = (Map.Entry)iterator.next();
            Configuration configuration = new Configuration();
            configuration.setKey((String)pair.getKey());
            configuration.setValue((String)pair.getValue());
            configurationList.add(configuration);
        }
        return configurationList;
    }
}
