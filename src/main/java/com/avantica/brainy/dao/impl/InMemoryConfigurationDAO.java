package com.avantica.brainy.dao.impl;

import com.avantica.brainy.beans.Configuration;
import com.avantica.brainy.dao.ConfigurationDAO;
import com.avantica.brainy.utils.ConfigurationContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("configurationDAO")
public class InMemoryConfigurationDAO implements ConfigurationDAO {

    private final ConfigurationContainer configurationContainer;

    @Autowired
    public InMemoryConfigurationDAO(ConfigurationContainer configurationContainer) {
        this.configurationContainer = configurationContainer;
    }

    @Override
    public boolean updateAllConfiguration(List<Configuration> configurationList) {
        return configurationContainer.updateAll(configurationList);

    }

    @Override
    public List<Configuration> getAllConfigurations() {
        return configurationContainer.getAll();
    }
}
