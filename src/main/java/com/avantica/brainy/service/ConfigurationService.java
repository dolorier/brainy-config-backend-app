package com.avantica.brainy.service;

import com.avantica.brainy.beans.Configuration;

import java.util.List;

public interface ConfigurationService {

    List<Configuration> getAllConfigurations();

    boolean updateAllConfiguration(List<Configuration> configurationList);

}