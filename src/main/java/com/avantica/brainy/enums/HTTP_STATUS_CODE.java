package com.avantica.brainy.enums;

/**
 * Created by diego.arguelles on 09/06/2016.
 */
public enum HTTP_STATUS_CODE {

    SERVER_ERROR(500),
    NOT_FOUND(400),
    SUCCESS(200);

    private Integer code;

    HTTP_STATUS_CODE(Integer code) {
        this.code=code;
    }

    public Integer getCode() {
        return code;
    }
}
