package com.avantica.brainy.beans;

import java.io.Serializable;

public class Configuration implements Serializable {

    private static final long serialVersionUID = 6335899030940805880L;

    private String key;
    private String value;

    public Configuration()  {}

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "key='" + key + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
