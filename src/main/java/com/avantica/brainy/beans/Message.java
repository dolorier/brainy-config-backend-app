package com.avantica.brainy.beans;


import org.springframework.http.HttpStatus;

import java.io.Serializable;

public class Message implements Serializable{

    private static final long serialVersionUID = 6335899030940805881L;

    private String description;
    private Integer code;

    public Message(String description, Integer code) {
        this.description = description;
        this.code = code;
    }

    public Message(HttpStatus httpStatus) {
        this.description = httpStatus.getReasonPhrase();
        this.code = httpStatus.value();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "Message{" +
                "description='" + description + '\'' +
                ", code=" + code +
                '}';
    }
}